﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace IniTool {
    class Ini {
        public class Entry {
            public string section;
            public string key;
            public string value;
        }

        enum ParserState {
            Whitespace,
            Section,
            Key,
            Value,
            Comment
        }

        List<Entry> entries = new List<Entry>();
        Dictionary<string, Entry> entriesByHash = new Dictionary<string, Entry>();

        public Regex allowDuplicatedKeys = null;

        public Ini() {

        }

        public void AddEntry(string section, string key, string value) {
            Entry current;

            if (allowDuplicatedKeys != null && allowDuplicatedKeys.IsMatch(key)) {
                current = new Entry() {
                    section = section,
                    key = key,
                    value = value
                };
                entries.Add(current);
            } else {
                var entryHash = section + "&&&" + key;
                if (entriesByHash.TryGetValue(entryHash, out current)) {
                    current.value = value;
                } else {
                    current = new Entry() {
                        section = section,
                        key = key,
                        value = value
                    };
                    entries.Add(current);
                    entriesByHash.Add(entryHash, current);
                }
            }
        }


        public Entry GetEntry(string section, string key) {
            var entryHash = section + "&&&" + key;
            Entry current;
            if (entriesByHash.TryGetValue(entryHash, out current)) {
                return current;
            }
            return null;
        }

        public override string ToString() {
            return ToString(false);
        }

        public string ToString(bool split) {
            StringBuilder result = new StringBuilder();
            string currentSection = "";
            for (int i = 0; i < entries.Count; ++i) {
                if (currentSection != entries[i].section) {
                    if (split) {
                        result.Append("\n");
                    }
                    result.Append("[" + entries[i].section + "]\n");
                    currentSection = entries[i].section;
                }
                result.Append(entries[i].key + "=" + entries[i].value + "\n");
            }
            return result.ToString();
        }

        public string GetValue(string section, string key) {
            var e = GetEntry(section, key);
            if (e != null) {
                return e.value;
            } else {
                return null;
            }
        }

        public void Clear() {
            entries.Clear();
            entriesByHash.Clear();
        }

        public void Append(string text) {
            ParserState state = ParserState.Whitespace;
            string buffer = "";
            string currentSection = "";
            string currentKey = "";
            string currentValue = "";
            bool escaping = false;

            for (int i = 0; i < text.Length; ++i) {
                if (state != ParserState.Comment) {
                    if (escaping) {
                        escaping = false;
                    } else if (text[i] == '\\') {
                        escaping = true;
                        continue;
                    }
                }

                if (state == ParserState.Whitespace) {
                    if (text[i] == '[') {
                        buffer = "";
                        state = ParserState.Section;
                    } else if (text[i] == ';' || text[i] == '#') {
                        state = ParserState.Comment;
                    } else if (!char.IsWhiteSpace(text[i])) {
                        buffer = "";
                        buffer += text[i];
                        currentKey = "";
                        state = ParserState.Key;
                    }
                } else if (state == ParserState.Section) {
                    if (text[i] == ']') {
                        currentSection = buffer;
                        state = ParserState.Whitespace;
                    } else {
                        buffer += text[i];
                    }
                } else if (state == ParserState.Comment) {
                    if (text[i] == '\n') {
                        state = ParserState.Whitespace;
                    }
                } else if (state == ParserState.Key) {
                    if (text[i] == '=') {
                        if (currentKey == "") {
                            currentKey = buffer;
                        }
                        buffer = "";
                        currentValue = "";
                        state = ParserState.Value;
                    } else if (text[i] == '\n') {
                        AddEntry(currentSection, currentKey, "");
                        state = ParserState.Whitespace;
                    } else if (char.IsWhiteSpace(text[i])) {
                        if (currentKey == "") {
                            currentKey = buffer;
                        }
                    } else {
                        buffer += text[i];
                    }
                } else if (state == ParserState.Value) {
                    if (text[i] == '\n' || i == text.Length - 1) {
                        if (currentValue == "") {
                            if (i == text.Length - 1) {
                                buffer += text[i];
                            }
                            currentValue = buffer;
                        }
                        AddEntry(currentSection, currentKey, currentValue);
                        state = ParserState.Whitespace;
                    } else if (char.IsWhiteSpace(text[i])) {
                        if (buffer != "") {
                            buffer += text[i];
                        }
                    } else {
                        buffer += text[i];
                    }
                }
            }
        }
    }
}
