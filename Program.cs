﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniTool {

    class ArgException : Exception {

    }

    class Program {
        static void Main(string[] args) {
            Ini baseIni = new Ini();
            bool splitSections = false;

            try {
                for (int i = 0; i < args.Length; ++i) {
                    if (args[i] == "-i" || args[i] == "/i") {
                        if (args.Length > i + 1) {
                            int numFiles = 0;
                            if (int.TryParse(args[i + 1], out numFiles)) {
                                if (args.Length > i + numFiles + 1) {
                                    for (int j = 0; j < numFiles; ++j) {
                                        var text = File.ReadAllText(args[i + 2 + j]);
                                        baseIni.Append(text);
                                    }
                                    i += numFiles + 1;
                                } else {
                                    throw new ArgException();
                                }
                            } else {
                                throw new ArgException();
                            }
                        } else {
                            throw new ArgException();
                        }
                    } else if (args[i] == "-s" || args[i] == "/s") {
                        splitSections = true;
                    } else if (args[i] == "-d" || args[i] == "/d") {
                        if (args.Length > i + 1) {
                            baseIni.allowDuplicatedKeys = new Regex(args[i + 1]);
                            ++i;
                        } else {
                            throw new ArgException();
                        }
                    } else if (args[i] == "-p" || args[i] == "/p") {
                        Console.Write(baseIni.ToString(splitSections));
                    } else if (args[i] == "-o" || args[i] == "/o") {
                        if (args.Length > i + 1) {
                            File.WriteAllText(args[i + 1], baseIni.ToString(splitSections));
                            ++i;
                        } else {
                            throw new ArgException();
                        }
                    } else if (args[i] == "-v" || args[i] == "/v") {
                        Console.WriteLine("initool 0.1");
                    } else {
                        Console.WriteLine("Unknown paramter: " + args[i]);
                    }
                }
                if (args.Length == 0) {
                    throw new ArgException();
                }
            } catch (ArgException e) {
                PrintHelp();
            }
        }

        static void PrintHelp() {
            Console.WriteLine("initool [-a][-s] -i <num_files> <file0> <file1> ... [-p] -o <out_file>");
            Console.WriteLine("");
            Console.WriteLine("-d regex\tAllow duplicated keys matching regex");
            Console.WriteLine("-s\tSplit sections with newline");
            Console.WriteLine("-i num file0 file1 ...\tList of input files");
            Console.WriteLine("-p\tPrint to stdout");
            Console.WriteLine("-o path\tOutput file");
        }
    }
}
